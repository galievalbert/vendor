package de.taxcalc.stock.model;

import de.taxcalc.core.model.Commodity_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-16T13:47:48.533+0200")
@StaticMetamodel(StockItem.class)
public class Article_ extends Commodity_ {
	public static volatile SingularAttribute<StockItem, Long> id;
	public static volatile SingularAttribute<StockItem, Price> price;
}
