package de.taxcalc.vendition.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-16T13:43:26.806+0200")
@StaticMetamodel(Receipt.class)
public class Receipt_ {
	public static volatile SingularAttribute<Receipt, Long> id;
	public static volatile SingularAttribute<Receipt, String> receiptToken;
	public static volatile SingularAttribute<Receipt, Date> startDate;
	public static volatile SingularAttribute<Receipt, Date> finishDate;
	public static volatile ListAttribute<Receipt, ReceiptRow> purchase;
	public static volatile SingularAttribute<Receipt, TransactionType> rType;
}
