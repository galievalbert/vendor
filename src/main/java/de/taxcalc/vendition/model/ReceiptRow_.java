package de.taxcalc.vendition.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-16T13:55:20.889+0200")
@StaticMetamodel(ReceiptRow.class)
public class ReceiptRow_ {
	public static volatile SingularAttribute<ReceiptRow, Long> id;
	public static volatile SingularAttribute<ReceiptRow, BigDecimal> netValue;
	public static volatile SingularAttribute<ReceiptRow, String> taxLable;
	public static volatile SingularAttribute<ReceiptRow, Integer> count;
	public static volatile SingularAttribute<ReceiptRow, String> articleName;
}
