package de.taxcalc.core.model;

import java.math.BigDecimal;

public enum SalesTax {	
	TAX_19("UmSt. 19%", BigDecimal.valueOf(19)), 
	TAX_07("UmSt. 7%", BigDecimal.valueOf(19));
	
	private String taxLable;
	private BigDecimal taxRate;
	
	SalesTax(String taxLable, BigDecimal taxRate) {
		this.taxLable = taxLable;
		this.taxRate = taxRate;
	}

	public String getTaxLable() {
		return taxLable;
	}

	public BigDecimal getTaxRate() {
		return taxRate;
	}
	
	public static SalesTax findByLable(String lable) throws Exception {
		for(SalesTax sTax : values()) {
			if(sTax.taxLable.equals(lable)) {
				return sTax;
			}
		}
		throw new Exception("Tax does not exist for lable "+lable);
	}
}
