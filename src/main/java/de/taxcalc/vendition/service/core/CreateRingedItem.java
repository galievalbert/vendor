package de.taxcalc.vendition.service.core;

import de.taxcalc.core.model.Rate;
import de.taxcalc.core.model.SalesTax;
import de.taxcalc.stock.model.Localization;
import de.taxcalc.stock.service.core.LoadRate;
import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RingedItem;

public class CreateRingedItem {

	private LoadRate loadRate = new LoadRate();
	
	public RingedItem runService(ReceiptRow rRow, Localization localization) throws Exception {
		Rate rate = loadRate.runService(rRow.getArticleNo(), localization);
		RingedItem rItem = new RingedItem(rRow.getArticleName(), rRow.getArticleNo(),
				SalesTax.findByLable(rRow.getTaxLable()), rRow.getCount(), rate);
		return rItem;
	}
}
