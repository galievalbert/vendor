package de.taxcalc.stock.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Price {

	@Id
	@SequenceGenerator(name = "price_id_sequence_generator", sequenceName = "price_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "price_id_sequence_generator")
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	private BigDecimal netAmount;

	@Column(name = "valid_by", columnDefinition = "TIMESTAMP")
	private LocalDateTime validBy;

	@Column(name = "invalid_by", columnDefinition = "TIMESTAMP")
	private LocalDateTime invalidBy;

	public Price() {
		super();
	}

	public Price(BigDecimal netAmount, LocalDateTime validBy, LocalDateTime invalidBy) {
		super();
		this.netAmount = netAmount;
		this.validBy = validBy;
		this.invalidBy = invalidBy;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public LocalDateTime getValidBy() {
		return validBy;
	}

	public LocalDateTime getInvalidBy() {
		return invalidBy;
	}

	public Long getId() {
		return id;
	}

}
