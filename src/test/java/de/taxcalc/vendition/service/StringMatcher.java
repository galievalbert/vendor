package de.taxcalc.vendition.service;

import org.mockito.ArgumentMatcher;

public class StringMatcher implements ArgumentMatcher<String>{

	public boolean matches(String argument) {
		try {
		Long.valueOf(argument);
		}catch (NumberFormatException exc){
			return false;
		}
		return true;
	}

}
