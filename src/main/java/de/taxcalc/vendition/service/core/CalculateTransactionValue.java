package de.taxcalc.vendition.service.core;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import de.taxcalc.stock.model.Localization;
import de.taxcalc.vendition.model.Receipt;
import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RowValue;
import de.taxcalc.vendition.model.TransactionValue;

public class CalculateTransactionValue {
	
	private CalculateRowValue calculateRowValue = new CalculateRowValue();

	private static final BiFunction<BigDecimal, BigDecimal, BigDecimal> func = (x1, x2) -> x1.add(x2);

	public TransactionValue runServise(Receipt receipt, Localization localization) throws Exception {
		BigDecimal totalNet = BigDecimal.ZERO;
		BigDecimal totalGross = BigDecimal.ZERO;
		Map<String, BigDecimal> taxes = new HashMap<String, BigDecimal>();
		Map<String, BigDecimal> netRowAmounts = new HashMap<String, BigDecimal>();
		List<RowValue> rValues = new LinkedList<RowValue>();
		for (ReceiptRow rRow : receipt.getPurchase()) {
			RowValue rValue = calculateRowValue.runService(rRow, localization);
			taxes.merge(rRow.getTaxLable(), rValue.getTaxTotal(), func);
			totalNet = totalNet.add(rValue.getNetTotal());
			netRowAmounts.merge(rRow.getTaxLable(), rValue.getNetTotal(), func);
			totalGross = totalGross.add(rValue.getGrossTotal());
			rValues.add(rValue);
		}
		return new TransactionValue(totalNet, totalGross, taxes, netRowAmounts, rValues, receipt.getId());
	}
}
