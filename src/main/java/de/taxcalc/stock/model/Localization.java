package de.taxcalc.stock.model;

import java.math.RoundingMode;
import java.util.Locale;

public enum Localization {
	LOCALIZATION_DE(2, RoundingMode.HALF_UP, "de", "DE"), 
	LOCALIZATION_BG(2, RoundingMode.HALF_UP, "bg", "BG");
	
	public final Integer SCALE;
	public final Locale LOCALE;
	public final RoundingMode RMODE;
	
	private Localization(int scale, RoundingMode rMode, String country, String language) {
		this.SCALE = scale;
		this.LOCALE = new Locale(country, language);
		RMODE = rMode;
	}
}
