package de.taxcalc.vendition.runner;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import de.taxcalc.stock.model.Localization;
import de.taxcalc.stock.model.Price;

public class SampleApp {

	public static void main(String[] args) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("vendor");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();
		Price price = new Price(BigDecimal.valueOf(10), Localization.LOCALIZATION_DE, new Date(), new Date());

		entitymanager.persist(price);
		entitymanager.getTransaction().commit();

		entitymanager.close();
		emfactory.close();
	}
}
