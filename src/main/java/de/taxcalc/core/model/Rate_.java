package de.taxcalc.core.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-15T12:42:00.573+0200")
@StaticMetamodel(Rate.class)
public class Rate_ {
	public static volatile SingularAttribute<Rate, Long> id;
	public static volatile SingularAttribute<Rate, BigDecimal> netAmount;
}
