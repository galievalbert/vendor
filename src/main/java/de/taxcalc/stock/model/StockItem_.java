package de.taxcalc.stock.model;

import de.taxcalc.core.model.Commodity_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-21T14:55:36.030+0200")
@StaticMetamodel(StockItem.class)
public class StockItem_ extends Commodity_ {
	public static volatile SingularAttribute<StockItem, Long> id;
}
