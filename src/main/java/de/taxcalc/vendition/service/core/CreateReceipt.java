package de.taxcalc.vendition.service.core;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import de.taxcalc.stock.model.Localization;
import de.taxcalc.vendition.model.BusinessTransaction;
import de.taxcalc.vendition.model.Receipt;
import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RingedItem;
import de.taxcalc.vendition.model.TransactionType;
import de.taxcalc.vendition.model.TransactionValue;

public class CreateReceipt {
	
	public CreateReceipt() {
		
	}

	private CalculateTransactionValue calculateTransactionValue = new CalculateTransactionValue();
	public TransactionValue runService(BusinessTransaction bTransaction, Localization loc) throws Exception {
		List<ReceiptRow> rows = new LinkedList<ReceiptRow>();
		for(RingedItem rItem : bTransaction.getItems()) {
			ReceiptRow rRow = CreateReceiptRow.runService(rItem);
			rows.add(rRow);
		}
		Receipt receipt = new Receipt("sdfasdf", bTransaction.getStartDate(), new Date(), rows, TransactionType.TERMINAL_SALE);
		TransactionValue tValue = calculateTransactionValue.runServise(receipt, loc);
		return tValue;
	}
}
