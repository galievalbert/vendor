package de.taxcalc.vendition.model;

import java.math.BigDecimal;

public class RowValue {
	private RingedItem rItem;
	private BigDecimal taxTotal;
	private BigDecimal grossTotal;
	private BigDecimal netTotal;
	private Long receiptRowId;

	public RowValue(BigDecimal netTotal, BigDecimal taxTotal, BigDecimal grossTotal, Long receiptRowId, RingedItem rItem) {
		super();
		this.taxTotal = taxTotal;
		this.grossTotal = grossTotal;
		this.netTotal = netTotal;
		this.receiptRowId = receiptRowId;
		this.rItem = rItem;
	}

	public Long getReceiptRowId() {
		return receiptRowId;
	}

	public BigDecimal getGrossTotal() {
		return grossTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public RingedItem getrItem() {
		return rItem;
	}

}
