package de.taxcalc.vendition.service.core;

import java.math.BigDecimal;
import java.math.RoundingMode;

import de.taxcalc.core.model.Rate;
import de.taxcalc.core.model.SalesTax;
import de.taxcalc.stock.model.Localization;
import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RingedItem;
import de.taxcalc.vendition.model.RowValue;

public class CalculateRowValue {

	private static final BigDecimal HUNDRED = new BigDecimal("100");
	
	private CreateRingedItem createRingedItem = new CreateRingedItem();

	public RowValue runService(ReceiptRow rRow, Localization localization) throws Exception {	
		int scale = localization.SCALE;
		RoundingMode rMode = localization.RMODE;
		return calculateValues(rRow, localization, scale, rMode);
	}

	private BigDecimal calculateTaxValue(BigDecimal netAmount, SalesTax sTax, int rCount){
		return netAmount.multiply(sTax.getTaxRate()).divide(CalculateRowValue.HUNDRED).multiply(BigDecimal.valueOf(rCount));
	}

	private BigDecimal calculateGrossValue(BigDecimal netAmount, BigDecimal taxValue, int rCount) {
		return netAmount.multiply(BigDecimal.valueOf(rCount)).add(taxValue);
	}
	
	private BigDecimal calculateNetValue(BigDecimal netAmount, int rCount) {
		return netAmount.multiply(BigDecimal.valueOf(rCount));
	}
	
	private RowValue calculateValues(ReceiptRow rRow, Localization localization, int scale, RoundingMode rMode) throws Exception {
		SalesTax sTax = SalesTax.findByLable(rRow.getTaxLable());
		int rCount = rRow.getCount();
		
		BigDecimal netAmount = rRow.getNetValue();
		BigDecimal taxTotal = calculateTaxValue(netAmount, sTax, rCount);
		BigDecimal grossTotal = calculateGrossValue(netAmount, taxTotal, rCount);
		BigDecimal netTotal = calculateNetValue(netAmount, rCount);
		RingedItem rItem = createRingedItem.runService(rRow, Localization.LOCALIZATION_DE);
		return new RowValue(netTotal, taxTotal, grossTotal, rRow.getId(), rItem);
	}
}
