package de.taxcalc.vendition.model;

import de.taxcalc.core.model.Rate;
import de.taxcalc.core.model.SalesTax;

public class RingedItem{   
	private String name;
	private String articleNo;
	private SalesTax sTax;
	private int count;
	private Rate rate;
	
	public RingedItem(String name, String articleNo, SalesTax sTax, int count, Rate rate) {
		super();
		this.name = name;
		this.articleNo = articleNo;
		this.sTax = sTax;
		this.count = count;
		this.rate = rate;
	}

	public String getName() {
		return name;
	}

	public String getArticleNo() {
		return articleNo;
	}

	public SalesTax getsTax() {
		return sTax;
	}

	public int getCount() {
		return count;
	}

	public Rate getRate() {
		return rate;
	}
	
}
