package de.taxcalc.core.model;

import javax.persistence.MappedSuperclass;

import de.taxcalc.stock.model.Price;

@MappedSuperclass
public abstract class Commodity {
	protected String name;
	protected String articleNo;
	protected SalesTax sTax;
	protected String description;
	protected Price price;

	public Commodity() {

	}

	public Commodity(String name, String articleNo, SalesTax sTax, String description, Price price) {
		super();
		this.name = name;
		this.articleNo = articleNo;
		this.sTax = sTax;
		this.description = description;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public String getArticleNo() {
		return articleNo;
	}

	public SalesTax getsTax() {
		return sTax;
	}

	public String getDescription() {
		return description;
	}

	public Price getPrice() {
		return price;
	} 
}
