package de.taxcalc.core.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-16T13:47:48.531+0200")
@StaticMetamodel(Commodity.class)
public class Commodity_ {
	public static volatile SingularAttribute<Commodity, String> name;
	public static volatile SingularAttribute<Commodity, String> articleNo;
	public static volatile SingularAttribute<Commodity, SalesTax> sTax;
}
