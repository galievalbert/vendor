package de.taxcalc.vendition.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-16T13:51:50.899+0200")
@StaticMetamodel(BusinessTransaction.class)
public class BusinessTransaction_ {
	public static volatile SingularAttribute<BusinessTransaction, Long> id;
	public static volatile SingularAttribute<BusinessTransaction, Date> startDate;
	public static volatile SingularAttribute<BusinessTransaction, String> counterId;
}
