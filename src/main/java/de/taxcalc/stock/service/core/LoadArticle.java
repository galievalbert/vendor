package de.taxcalc.stock.service.core;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import de.taxcalc.stock.model.StockItem;

public class LoadArticle {
	
	private EntityManagerFactory emfactory;
	private EntityManager entitymanager;
	
	public LoadArticle() {
		
	}
	
	public LoadArticle(EntityManagerFactory emfactory, EntityManager entitymanager) {
		super();
		this.emfactory = emfactory;
		this.entitymanager = entitymanager;
	}



	public StockItem runService(Long articleToken) {
		emfactory = Persistence.createEntityManagerFactory("vendor");
		entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();
		return entitymanager.find(StockItem.class, articleToken);
	}
}
