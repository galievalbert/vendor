package de.taxcalc.vendition.model;

import java.util.Date;
import java.util.List;

public class BusinessTransaction {

	private List<RingedItem> items;
	private Date startDate;
	private String counterId;
	
	public BusinessTransaction(Date startDate, String counterId, List<RingedItem>rItems) {
		super();
		this.startDate = startDate;
		this.counterId = counterId;
		this.items = rItems;
	}

	public List<RingedItem> getItems() {
		return items;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getCounterId() {
		return counterId;
	}
}
