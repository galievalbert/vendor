package de.taxcalc.vendition.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ReceiptRow{
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
	private BigDecimal netValue;
	private String taxLable;
	private int count;
	private String articleName;
	private String articleNo;

	public ReceiptRow() {
		
	}
	
	public ReceiptRow(BigDecimal netValue, int count, String taxLable, String articleName, String articleNo) {
		this.netValue = netValue;
		this.count = count;
		this.taxLable = taxLable;
		this.articleName = articleName;
		this.articleNo = articleNo;
	}

	public int getCount() {
		return count;
	}

	public BigDecimal getNetValue() {
		return netValue;
	}

	public String getTaxLable() {
		return taxLable;
	}

	public Long getId() {
		return id;
	}

	public String getArticleName() {
		return articleName;
	}

	public String getArticleNo() {
		return articleNo;
	}
}
