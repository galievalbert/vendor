package de.taxcalc.vendition.model;

public enum TransactionType {

	TERMINAL_SALE, TERMINAL_REFUND, TERMINAL_DAYEND, TERMINAL_DAYSTART;
}
