package de.taxcalc.vendition.service.core;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RingedItem;

public class CreateReceiptRow {
	public static ReceiptRow runService(RingedItem rItem) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("vendor");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();
		ReceiptRow rRow = new ReceiptRow(rItem.getRate().getNetAmount(), rItem.getCount(),
				rItem.getsTax().getTaxLable(), rItem.getName(), rItem.getArticleNo());

		entitymanager.persist(rRow);
		entitymanager.getTransaction().commit();

		entitymanager.close();
		emfactory.close();
		return rRow;
	}
}
