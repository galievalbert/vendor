package de.taxcalc.vendition.service;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;
import org.mockito.Spy;

import de.taxcalc.core.model.Rate;
import de.taxcalc.core.model.SalesTax;
import de.taxcalc.stock.model.Localization;
import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RingedItem;
import de.taxcalc.vendition.model.RowValue;
import de.taxcalc.vendition.service.core.CalculateRowValue;
import de.taxcalc.vendition.service.core.CreateReceiptRow;

public class CalculateRowValueTest {
	
	@Spy
	private CalculateRowValue calculateRowValue;

	private static final BigDecimal ONEHUNDERD = BigDecimal.valueOf(100);
	private static final int COUNT = 2;
	private static final SalesTax STAX = SalesTax.TAX_19;
	private static final Localization LOCL = Localization.LOCALIZATION_DE;
	private static final int SCALE = LOCL.SCALE;
	private static final BigDecimal NETVALUE = BigDecimal.valueOf(10).setScale(SCALE);

	@Test
	public void test() throws Exception {
		ReceiptRow rRow = createReceiptRow();
		RowValue rValue = calculateRowValue.runService(rRow, Localization.LOCALIZATION_DE);

		BigDecimal testNetTotal = BigDecimal.valueOf(10).multiply(BigDecimal.valueOf(COUNT)).setScale(SCALE);
		assertEquals(rValue.getNetTotal(), testNetTotal);

		BigDecimal testTaxTotal = createTaxTotal();
		assertEquals(rValue.getTaxTotal(), testTaxTotal);
		
		BigDecimal testGrossTotal = testNetTotal.add(testTaxTotal);
		assertEquals(rValue.getGrossTotal(), testGrossTotal);
	}

	private static ReceiptRow createReceiptRow() {
		Rate rate = new Rate(NETVALUE, Long.valueOf(131));
		RingedItem rItem = new RingedItem("test", "sd934", SalesTax.TAX_19, COUNT, rate);
		return CreateReceiptRow.runService(rItem);
	}

	private static BigDecimal createTaxTotal() {
		return NETVALUE.multiply(STAX.getTaxRate()).divide(ONEHUNDERD).multiply(BigDecimal.valueOf(COUNT));
	}

}
