package de.taxcalc.stock.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2021-07-15T12:42:00.578+0200")
@StaticMetamodel(Price.class)
public class Price_ {
	public static volatile SingularAttribute<Price, Long> id;
	public static volatile SingularAttribute<Price, BigDecimal> netAmount;
	public static volatile SingularAttribute<Price, Date> validBy;
	public static volatile SingularAttribute<Price, Date> invalidBy;
}
