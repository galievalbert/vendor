package de.taxcalc.vendition.service.core;

import java.util.Date;
import java.util.LinkedList;

import de.taxcalc.stock.model.Localization;
import de.taxcalc.vendition.model.BusinessTransaction;
import de.taxcalc.vendition.model.RingMode;
import de.taxcalc.vendition.model.RingedItem;
import de.taxcalc.vendition.model.TransactionType;
import de.taxcalc.vendition.model.TransactionValue;

public class ProcessVend {

	public static BusinessTransaction runService(int terminal, TransactionType tType) {
		BusinessTransaction bTransaction = new BusinessTransaction(new Date(), String.valueOf(terminal), new LinkedList<RingedItem>());
		return bTransaction;
	}

	public static void ringItem(BusinessTransaction bTransaction, RingMode rMode, RingedItem rItem) {
		bTransaction.getItems().add(rItem);
	}
	
	public static TransactionValue finishReceipt(BusinessTransaction bTransaction) throws Exception {
		return CreateReceipt.runService(bTransaction, Localization.LOCALIZATION_DE);
	}
}
