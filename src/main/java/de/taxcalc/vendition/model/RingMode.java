package de.taxcalc.vendition.model;

public enum RingMode {
	ITEM_CANCEL, ITEM_SELL;
}
