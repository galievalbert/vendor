package de.taxcalc.vendition.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.mockito.junit.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import de.taxcalc.core.model.Rate;
import de.taxcalc.core.model.SalesTax;
import de.taxcalc.stock.model.StockItem;
import de.taxcalc.stock.model.Localization;
import de.taxcalc.stock.model.Price;
import de.taxcalc.stock.service.core.LoadArticle;
import de.taxcalc.stock.service.core.LoadRate;
import de.taxcalc.vendition.model.BusinessTransaction;
import de.taxcalc.vendition.model.RingMode;
import de.taxcalc.vendition.model.RingedItem;
import de.taxcalc.vendition.model.TransactionType;
import de.taxcalc.vendition.model.TransactionValue;
import de.taxcalc.vendition.service.core.CreateReceipt;
import de.taxcalc.vendition.service.core.ProcessVend;

@RunWith(MockitoJUnitRunner.class)
public class ProcessVendTest {

	@Mock
	private StockItem article;
	
	@Mock
	private EntityManager entityManager;

	@Mock
	private EntityManagerFactory eManagerFactory;

	@Mock
	private EntityManager eManager;
	
	@InjectMocks
	private LoadArticle loadArticle;
	
	@InjectMocks
	private LoadRate loadRate;
	
	@InjectMocks
	private CreateReceipt createReceipt;

	private static final Localization LOCL = Localization.LOCALIZATION_DE;
	private static final int SCALE = LOCL.SCALE;
	private static final BigDecimal NETVALUE = BigDecimal.valueOf(10).setScale(SCALE);

	@Test
	public void test() throws Exception {
		Price price = new Price(NETVALUE, LocalDateTime.of(21, 3, 21, 12, 41, 53), LocalDateTime.of(21, 5, 21, 12, 41, 53));
		when(article.getPrice()).thenReturn(price);
		doReturn(article).when(loadArticle).runService(Mockito.anyLong());
		BusinessTransaction bTransaction = ProcessVend.runService(1, TransactionType.TERMINAL_SALE);
		Rate rate = new Rate(BigDecimal.valueOf(10), Long.valueOf(111));
		RingedItem rItem = new RingedItem("Handy", "01943591", SalesTax.TAX_07, 17, rate);
		ProcessVend.ringItem(bTransaction, RingMode.ITEM_SELL, rItem);
		rItem = new RingedItem("Stuhl", "01443591", SalesTax.TAX_19, 32, rate);
		ProcessVend.ringItem(bTransaction, RingMode.ITEM_SELL, rItem);
		TransactionValue tValue = createReceipt.runService(bTransaction, Localization.LOCALIZATION_DE);
		assertEquals(2, tValue.getNetValues().keySet().size());
		tValue = null;
	}
}
