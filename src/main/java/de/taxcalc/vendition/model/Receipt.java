package de.taxcalc.vendition.model;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Receipt {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String receiptToken;
    
    @Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
    
    @Temporal(TemporalType.TIMESTAMP)
	private Date finishDate;
	
	@OneToMany
	private List<ReceiptRow> purchase;
	private TransactionType rType;
	
	public Receipt() {
		
	}

	public Receipt(String receiptToken, Date startDate, Date finishDate, List<ReceiptRow> purchase, TransactionType rType) {
		super();
		this.receiptToken = receiptToken;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.purchase = Collections.unmodifiableList(purchase);
		this.rType = rType;
	}

	public String getReceiptToken() {
		return receiptToken;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public List<ReceiptRow> getPurchase() {
		return purchase;
	}

	public TransactionType getrType() {
		return rType;
	}

	public Long getId() {
		return id;
	}
}
