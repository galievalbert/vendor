package de.taxcalc.stock.service.core;

import java.math.BigDecimal;

import de.taxcalc.core.model.Rate;
import de.taxcalc.stock.model.StockItem;
import de.taxcalc.stock.model.Localization;
import de.taxcalc.stock.model.Price;

public class LoadRate {

	private LoadArticle loadArticle;
	
	public LoadRate() {
		loadArticle = new LoadArticle();
	}
	
	public LoadRate(LoadArticle loadArticle) {
		super();
		this.loadArticle = loadArticle;
	}

	public Rate runService(String articleToken, Localization loc) {
		StockItem article = loadArticle.runService(Long.valueOf(articleToken));
		Price price = article.getPrice();
		BigDecimal netAmount = price.getNetAmount().setScale(loc.SCALE, loc.RMODE);
		Rate rate = new Rate(netAmount, price.getId());
		return rate;
	}
}
