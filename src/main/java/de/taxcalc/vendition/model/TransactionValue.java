package de.taxcalc.vendition.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionValue {
	private BigDecimal totalNet;
	private BigDecimal totalGross;
	private Map<String, BigDecimal> taxes = new HashMap<String, BigDecimal>();
	private Map<String, BigDecimal> netValues = new HashMap<String, BigDecimal>();
	private List<RowValue> rowValues;
	private Long receiptId;
	
	public TransactionValue(BigDecimal totalNet, BigDecimal totalGross, Map<String, BigDecimal> taxes,
			Map<String, BigDecimal> netValues, List<RowValue> rowValues, Long receiptId) {
		super();
		this.totalNet = totalNet;
		this.totalGross = totalGross;
		this.taxes = taxes;
		this.netValues = netValues;
		this.rowValues = rowValues;
		this.receiptId = receiptId;
	}

	public BigDecimal getTotalNet() {
		return totalNet;
	}

	public BigDecimal getTotalGross() {
		return totalGross;
	}

	public Map<String, BigDecimal> getTaxes() {
		return taxes;
	}

	public Map<String, BigDecimal> getNetValues() {
		return netValues;
	}

	public Long getReceiptId() {
		return receiptId;
	}
	
	
}
