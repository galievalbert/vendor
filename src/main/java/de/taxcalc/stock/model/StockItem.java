package de.taxcalc.stock.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import de.taxcalc.core.model.Commodity;
import de.taxcalc.core.model.SalesTax;

@Entity
public class StockItem extends Commodity{
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	public StockItem() {
		super();
	}

	public StockItem(String name, String articleNo, SalesTax sTax, String description, Price price) {
		super(name, articleNo, sTax, description, price);
	}

	public Long getId() {
		return id;
	}
}
