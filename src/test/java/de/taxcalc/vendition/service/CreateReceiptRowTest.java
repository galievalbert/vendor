package de.taxcalc.vendition.service;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import de.taxcalc.core.model.Rate;
import de.taxcalc.core.model.SalesTax;
import de.taxcalc.stock.model.Localization;
import de.taxcalc.vendition.model.ReceiptRow;
import de.taxcalc.vendition.model.RingedItem;
import de.taxcalc.vendition.service.core.CreateReceiptRow;

public class CreateReceiptRowTest {
	
	private static final Localization LOCL = Localization.LOCALIZATION_DE;
	private static final int SCALE = LOCL.SCALE;
	private static final BigDecimal NETVALUE = BigDecimal.valueOf(10).setScale(SCALE);

	@Test
	public void test() {
		Rate rate = new Rate(NETVALUE, Long.valueOf(131));
		RingedItem rItem = new RingedItem("test", "sd934", SalesTax.TAX_19, 2, rate);
		ReceiptRow rRow = CreateReceiptRow.runService(rItem);
		assertEquals(rRow.getNetValue(), NETVALUE);
		assertEquals(rRow.getArticleName(), "test");
		assertEquals(rRow.getCount(), 2);
	}
}
