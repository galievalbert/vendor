package de.taxcalc.core.model;

import java.math.BigDecimal;

public class Rate {

	private Long priceId;
    
	private BigDecimal netAmount;

	public Rate(BigDecimal netAmount, Long priceId) {
		super();
		this.netAmount = netAmount;
		this.priceId = priceId;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public Long getPriceId() {
		return priceId;
	}
}
